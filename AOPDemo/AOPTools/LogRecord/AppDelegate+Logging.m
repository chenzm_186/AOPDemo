//
//  AppDelegate+Logging.m
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "AppDelegate+Logging.h"
#import "ZMLogging.h"


@implementation AppDelegate (Logging)

- (void)setupLogging{
    NSDictionary *config =
  @{
    @"ZMSecVC": @{
         ZMLoggingPageImpression: @"page imp - ZMSecVC page",
         ZMLoggingTrackedEvents: @[
             @{
                 ZMLoggingEventName: @"button 1 clicked",
                 ZMLoggingEventSelectorName: @"btnAction1:",
                 ZMLoggingEventHandlerBlock: ^(id<AspectInfo> aspectInfo) {
                     NSLog(@"btnAction1");
                 },
                 },
             @{
                 ZMLoggingEventName: @"button 2 clicked",
                 ZMLoggingEventSelectorName: @"btnAction2:",
                 ZMLoggingEventHandlerBlock: ^(id<AspectInfo> aspectInfo) {
                     NSLog(@"btnAction2");
                 },
                 },
             ],
         },
    
    @"ZMThiVC": @{
         ZMLoggingPageImpression: @"page imp - ZMThiVC page",
         },
    @"ZMMenuView":@{
        ZMLoggingTrackedEvents: @[
                @{
                    ZMLoggingEventName: @"ZMMenuView",
                    ZMLoggingEventSelectorName: @"menuButtonClick:",
                    ZMLoggingEventHandlerBlock: ^(id<AspectInfo> aspectInfo) {
                        NSLog(@"menuButtonClick");
                    },
                },
            ],
        }
     };
    
    [ZMLogging setupWithConfiguration:config];
}


@end
