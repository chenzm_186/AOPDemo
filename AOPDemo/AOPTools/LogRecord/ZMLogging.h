//
//  ZMLogging.h
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Aspects.h>

#define ZMLoggingPageImpression @"ZMLoggingPageImpression"
#define ZMLoggingTrackedEvents @"ZMLoggingTrackedEvents"
#define ZMLoggingEventName @"ZMLoggingEventName"
#define ZMLoggingEventSelectorName @"ZMLoggingEventSelectorName"
#define ZMLoggingEventHandlerBlock @"ZMLoggingEventHandlerBlock"


@interface ZMLogging : NSObject


+ (void)setupWithConfiguration:(NSDictionary *)configs;


@end
