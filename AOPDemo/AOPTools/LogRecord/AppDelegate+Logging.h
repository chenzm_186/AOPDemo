//
//  AppDelegate+Logging.h
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Logging)

- (void)setupLogging;


@end
