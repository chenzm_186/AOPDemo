//
//  ZMMobClickTool.m
//  GuaWaProject
//
//  Created by chenzm on 2018/8/28.
//  Copyright © 2018年 木炎. All rights reserved.
//

//【计算事件】 用于统计数值型变量的累计值、均值及分布。
//【计数事件】 用于统计字符串型变量的消息数及触发设备数。


#import "ZMMobClickTool.h"
#import <UMAnalytics/MobClick.h>


@implementation ZMMobClickTool

///开始页面统计
+(void)zm_beginLogPageView:(NSString *)pageName{
    if (!AnaIsEmptyStr(pageName)) {
        [MobClick beginLogPageView:pageName];
    }
}

///结束页面统计
+ (void)zm_endLogPageView:(NSString *)pageName{
    if (!AnaIsEmptyStr(pageName)) {
        [MobClick endLogPageView:pageName];
    }
}

/**
 时长统计[开始]
 @param eventId 注册的事件Id
 @param dic 不能超过10个，key－value必须为String类型
 @param lblStr 分类标签。不同的标签会分别进行统计
 */
+(void)zm_beginEventId:(NSString *)eventId dic:(NSDictionary *)dic lblStr:(NSString *)lblStr{
    if (!AnaIsEmptyDic(dic)) {
        [MobClick beginEvent:eventId primarykey:eventId attributes:dic];
    }else{
        if (!AnaIsEmptyStr(lblStr)) {
            [MobClick beginEvent:eventId label:lblStr];
        }else{
            [MobClick beginEvent:eventId];
        }
    }
}

/**
 时长统计[结束]
 @param eventId 注册的事件Id
 @param dic 不能超过10个，key－value必须为String类型
 @param lblStr 分类标签。不同的标签会分别进行统计
 */
+(void)zm_endEventId:(NSString *)eventId dic:(NSDictionary *)dic lblStr:(NSString *)lblStr{
    if (!AnaIsEmptyDic(dic)) {
        [MobClick endEvent:eventId primarykey:eventId];
    }else{
        if (!AnaIsEmptyStr(lblStr)) {
            [MobClick endEvent:eventId label:lblStr];
        }else{
            [MobClick endEvent:eventId];
        }
    }
}

/**
 数量统计
 */
+(void)zm_countEventId:(NSString *)eventId dic:(NSDictionary *)dic lblStr:(NSString *)lblStr{
    if (!AnaIsEmptyDic(dic)) {
        [MobClick event:eventId attributes:dic];
    }else{
        if (!AnaIsEmptyStr(lblStr)) {
            [MobClick event:eventId label:lblStr];
        }else{
            [MobClick event:eventId];
        }
    }
}

@end
