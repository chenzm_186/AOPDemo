//
//  ZMMobClickTool.h
//  GuaWaProject
//
//  Created by chenzm on 2018/8/28.
//  Copyright © 2018年 木炎. All rights reserved.
//

#import <Foundation/Foundation.h>

// 字典
#define AnaIsEmptyDic(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)
// 字符串
#define AnaIsEmptyStr(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )

@interface ZMMobClickTool : NSObject

///开始页面统计
+(void)zm_beginLogPageView:(NSString *)pageName;

///结束页面统计
+ (void)zm_endLogPageView:(NSString *)pageName;

/**
 数量统计
 @param eventId 注册的事件Id
 @param dic 不能超过10个，key－value必须为String类型
 @param lblStr 分类标签。不同的标签会分别进行统计
 */
+(void)zm_countEventId:(NSString *)eventId dic:(NSDictionary *)dic lblStr:(NSString *)lblStr;

/**
 时长统计[开始]
 @param eventId 注册的事件Id
 @param dic 不能超过10个，key－value必须为String类型
 @param lblStr 分类标签。不同的标签会分别进行统计
 */
+(void)zm_beginEventId:(NSString *)eventId dic:(NSDictionary *)dic lblStr:(NSString *)lblStr;

/**
 时长统计[结束]
 @param eventId 注册的事件Id
 @param dic 不能超过10个，key－value必须为String类型
 @param lblStr 分类标签。不同的标签会分别进行统计
 */
+(void)zm_endEventId:(NSString *)eventId dic:(NSDictionary *)dic lblStr:(NSString *)lblStr;


@end
