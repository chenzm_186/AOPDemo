//
//  AppDelegate+AopUMStatistical.h
//  GuaWaProject
//
//  Created by chenzm on 2018/8/28.
//  Copyright © 2018年 木炎. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (AopUMStatistical)

///友盟分析
- (void)configureUMTrack;

///设置事件分析
- (void)setUpAnalytics;


@end
