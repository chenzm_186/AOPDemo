//
//  AppDelegate+SmallFeature.h
//  GuaWaProject
//
//  Created by chenzm on 2018/8/29.
//  Copyright © 2018年 木炎. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (SmallFeature)

///小功能添加
-(void)setSmallFeature;

@end
