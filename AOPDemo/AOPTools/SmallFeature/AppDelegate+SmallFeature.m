//
//  AppDelegate+SmallFeature.m
//  GuaWaProject
//
//  Created by chenzm on 2018/8/29.
//  Copyright © 2018年 木炎. All rights reserved.
//

#import "AppDelegate+SmallFeature.h"

#import <Aspects.h>

@implementation AppDelegate (SmallFeature)

///小功能添加
-(void)setSmallFeature{
    [self jumpToFirVC];
}

///跳转页面
-(void)jumpToFirVC{
    Class firVC = NSClassFromString(@"ZMFirVC");
    __weak typeof(self) weakSelf = self;
    SEL action = NSSelectorFromString(@"btnAction:");
    [firVC aspect_hookSelector:action withOptions:AspectPositionAfter usingBlock:^(id <AspectInfo> aspectInfo,UIButton *btn){
        
        Class snatchTreasure = NSClassFromString(@"ZMSecVC");
        [weakSelf.navigationController pushViewController:[snatchTreasure new] animated:YES];
    } error:NULL];
}


@end
