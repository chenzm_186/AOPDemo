//
//  ZMHomeVC.m
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "ZMHomeVC.h"
#import "ZMHomeTableView.h"


@interface ZMHomeVC ()

///表格
@property(nonatomic,strong)ZMHomeTableView *tableView;


@end

@implementation ZMHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setBaseUI];
}

-(void)setBaseUI{
    self.view.backgroundColor = [UIColor whiteColor];
    [self tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - lazyload

-(ZMHomeTableView *)tableView{
    if (!_tableView) {
        _tableView = [[ZMHomeTableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain];
        
        __weak typeof(self) weakSelf = self;
        _tableView.didSelectRowAction = ^(UIViewController *vc) {
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

@end
