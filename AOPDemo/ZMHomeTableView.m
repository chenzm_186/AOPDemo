//
//  ZMHomeTableView.m
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "ZMHomeTableView.h"

@interface ZMHomeTableView()<UITableViewDelegate,UITableViewDataSource>

///单元格标题数组
@property(nonatomic,strong)NSArray *titlesArr;

///控制器名字数组
@property(nonatomic,strong)NSArray *controllArr;


@end


@implementation ZMHomeTableView

#pragma mark - initial
-(instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self = [super initWithFrame:frame style:style]) {
        self.delegate = self;
        self.dataSource = self;
        self.tableFooterView = [UIView new];
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        [self registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellID"];
    }
    return self;
}


#pragma mark - delegate/datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titlesArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    cell.textLabel.text = self.titlesArr[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //跳转
    if (indexPath.row < self.controllArr.count) {
        UIViewController *vc = [NSClassFromString(self.controllArr[indexPath.row]) new];
        vc.navigationController.title = self.titlesArr[indexPath.row];
        
        !_didSelectRowAction?:_didSelectRowAction(vc);
    }
}

#pragma mark - lazyload

-(NSArray *)titlesArr{
    if (!_titlesArr) {
        _titlesArr = @[@"点击事件/统计分析",
                       @"日志打印",
                       @"标签栏"
                       ];
    }
    return _titlesArr;
}

-(NSArray *)controllArr{
    if (!_controllArr) {
        _controllArr = @[@"ZMFirVC",
                         @"ZMSecVC",
                         @"ZMThiVC"
                         ];
    }
    return _controllArr;
}



@end
