//
//  ZMThiVC.m
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "ZMThiVC.h"
#import "ZMMenuView.h"
#import <Aspects.h>
@interface ZMThiVC ()

///标签栏
@property(nonatomic,strong)ZMMenuView *menuView;

@end

@implementation ZMThiVC

#pragma mark - initial
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpBaseUI];
}

-(void)setUpBaseUI{
    [self menuView];
    //拦截menuView的dlMenuView:atIndex:方法
    //降低类与类的耦合度
    [_menuView aspect_hookSelector:NSSelectorFromString(@"dlMenuView:atIndex:") withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspects, ZMMenuView *menuView, NSInteger index){
         NSLog(@"按钮点击了 %ld",index);
     } error:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - lazyload
-(ZMMenuView *)menuView{
    if (!_menuView) {
        ZMMenuView *menu = [[ZMMenuView alloc] initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, 40)];
        //菜单栏标题
        menu.menuTitles = @[@"新闻",@"头条",@"好声音",@"原创",@"视频",@"土豆",@"优酷",@"科技",@"图片"];
        menu.backgroundColor = [UIColor yellowColor];
        //默认选择第1项
        menu.selectedIndex = 0;
        [menu setDLMenuViewDidClickAtIndexCallBack:^(NSInteger index) {
            NSLog(@"click %ld",index);
        }];
        _menuView = menu;
        [self.view addSubview:_menuView];
    }
    return _menuView;
}


@end
