//
//  ZMSecVC.m
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "ZMSecVC.h"
#import "ZMThiVC.h"

@interface ZMSecVC ()

///按钮
@property(nonatomic,strong)NSArray *titleArr;

@end

@implementation ZMSecVC


#pragma mark - initial
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"点击事件";
    [self setBaseUI];
}


-(void)setBaseUI{
    for (NSInteger i = 0; i < self.titleArr.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(50 + 100*2*i, 300, 100, 30);
        btn.tag = i + 1;
        [btn setTitle:self.titleArr[i] forState:UIControlStateNormal];
        btn.backgroundColor = [UIColor redColor];
        switch (btn.tag) {
            case 1:{
                [btn addTarget:self action:@selector(btnAction1:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            case 2:{
                [btn addTarget:self action:@selector(btnAction2:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            default:
                break;
        }
        [self.view addSubview:btn];
    }
    
}

#pragma mark - method

-(void)btnAction1:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
}

-(void)btnAction2:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
    
    ZMThiVC *vc = [ZMThiVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

///带参数
-(void)addAddress:(NSDictionary *)dic arr:(NSArray *)arr{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - lazyload
-(NSArray *)titleArr{
    if (!_titleArr) {
        _titleArr = @[@"按钮1",
                      @"按钮2"
                      ];
    }
    return _titleArr;
}

@end
