//
//  ZMHomeTableView.h
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZMHomeTableView : UITableView

///单元格点击代码块
@property(nonatomic,strong)void (^didSelectRowAction)(UIViewController *vc);



@end
