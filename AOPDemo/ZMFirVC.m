//
//  ZMFirVC.m
//  AOPDemo
//
//  Created by chenzm on 2018/8/24.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "ZMFirVC.h"
#import "NSMutableArray+Safe.h"


@interface ZMFirVC ()

///按钮
@property(nonatomic,strong)NSArray *titleArr;

///数组
@property(nonatomic,strong)NSMutableArray *marr;


@end

@implementation ZMFirVC


#pragma mark - initial
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"点击事件";
    [self setBaseUI];
}


-(void)setBaseUI{
    for (NSInteger i = 0; i < self.titleArr.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(200, 30*(i*2+4), 100, 30);
        btn.tag = i + 1;
        [btn setTitle:self.titleArr[i] forState:UIControlStateNormal];
        btn.backgroundColor = [UIColor redColor];
        switch (btn.tag) {
            case 1:{
                [btn addTarget:self action:@selector(btnAction1:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            case 2:{
                [btn addTarget:self action:@selector(btnAction2:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            case 3:{
                [btn addTarget:self action:@selector(btnAction3:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            case 4:{
                [btn addTarget:self action:@selector(btnAction4:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            case 5:{
                [btn addTarget:self action:@selector(btnAction5:) forControlEvents:UIControlEventTouchUpInside];
            }break;
            default:
                break;
        }
        [self.view addSubview:btn];
    }

}

#pragma mark - method

-(void)btnAction1:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
    [self testAction:@""];
}


-(void)testAction:(NSString *)str{
    NSLog(@"testAction");
}

-(void)btnAction2:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
}

-(void)btnAction3:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
}

-(void)btnAction4:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
}

-(void)btnAction5:(UIButton *)sender{
    NSLog(@"点击按钮tag：%ld",(long)sender.tag);
    
    NSDictionary *dic = @{@"address":@"地址",
                          @"name":@"名字"
                          };
    NSArray *arr = @[@"类标",@"类标",@"类标",@"类标"];
    [self addAddress:dic arr:arr];
}

///带参数
-(void)addAddress:(NSDictionary *)dic arr:(NSArray *)arr{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - lazyload
-(NSArray *)titleArr{
    if (!_titleArr) {
        _titleArr = @[@"按钮1",
                      @"按钮2",
                      @"按钮3",
                      @"按钮4",
                      @"按钮5"
                      ];
    }
    return _titleArr;
}

-(NSMutableArray *)marr{
    if (!_marr) {
        _marr = [NSMutableArray arrayWithCapacity:0];
    }
    return _marr;
}


@end
